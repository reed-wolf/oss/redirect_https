use actix_files as fs;
use actix_web::{middleware, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use http::Uri;
use log::debug;
use pretty_env_logger;

fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    pretty_env_logger::init();
    let bind_url =
        std::env::var("BIND_ADDR").unwrap_or_else(|_| panic!("{} must be set", "BIND_ADDR"));

    let static_dir =
        std::env::var("STATIC_DIR").unwrap_or_else(|_| panic!("{} must be set", "STATIC_DIR"));

    let wk_dir = format!("{}/.well-known/", static_dir.clone());
    std::fs::create_dir_all(wk_dir.clone()).unwrap_or_else(|e| panic!("{}", e));

    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .service(web::resource("/health").route(web::get().to(|| HttpResponse::Ok().finish())))
            .service(fs::Files::new("/.well-known/", wk_dir.clone()))
            .service(web::resource("/{any:.*}").route(web::get().to_async(redirect_to_https)))
    })
    .bind(bind_url)?
    .run()
}

fn redirect_to_https(req: HttpRequest) -> Result<HttpResponse, Error> {
    let mut temp = true;
    if let Ok(_mov) = std::env::var("MOVED_PERMANENTLY") {
        temp = false;
    }
    // let bind_url = std::env::var("BIND_ADDR").expect(&format!("{} must be set", "BIND_ADDR"));
    // let bind_uri = bind_url.parse::<Uri>().unwrap();
    let bind_uri = req.connection_info().clone();
    let uri = Uri::builder()
        .scheme("https")
        .authority(bind_uri.host())
        .path_and_query(req.path())
        .build()
        .unwrap();
    debug!("Redirect: {:?}", uri);
    let code = if temp {
        actix_web::http::StatusCode::TEMPORARY_REDIRECT
    } else {
        actix_web::http::StatusCode::MOVED_PERMANENTLY
    };
    Ok(HttpResponse::build(code)
        .header("location", uri.to_string())
        .finish())
}
