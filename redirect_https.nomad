job "redirect_https" {
  datacenters = ["dc1"]
  type = "service"

  group "bins" {
    count = 1
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }

    task "redirect_https" {
      driver = "raw_exec"

      config {
        # When running a binary that exists on the host, the path must be absolute.
        command = "/bin/sh"
        args    = ["-c", "cd /home/amocsy/redirect_https && exec /srv/rw/redirect_https/dev/redirect_https"]
      }

      // env {
      //   BIND_URL = "${NOMAD_ADDR_http}"
      // }

      resources {
        network {
          mbits = 1
          port "http" {
            static = 80
          }
        }
      }

      service {
        name = "redirect-https-service"
        tags = ["redirect_https", "rust"]
        port = "http"
        check {
          type     = "http"
          path     = "/health"
          interval = "60s"
          timeout  = "2s"
        }
      }
    }
  }
}