This program's primary purpose is to redirect all http requests to https with as little delay as possible on host:port BIND_ADDR.
It also serves files from /.well-known/ under STATIC_DIR to statisfy certbot's webroot challenge, but also useful for serving your public json web keys.

This appliance is inteded to be used with api/web gateways, proxies, reverse proxies and the like which are configured to listen only on 443 (for https).
Can be use alongside of nginx and friends, just let it bin on port 80.

# Start
This program doesn't acceppt command line parameters, all options are configurable with env vars or with .env file

# Certbot command to run
sudo certbot certonly --webroot -w /home/yourusernamehere/redirect_https/static/ -d youurfqgnhere -n --agree-tos -m EMAIL

# to run unpriviledged, use setcap
sudo setcap cap_net_bind_service=+ep redirect_https
./redirect_https &> redirect_https.log &

# To non-interactively renew *all* of your certificates, run
certbot renew
Note that if certbot is installed via deb, a systemd service takes care of this.
Keep redirect_https running, so renewals work fine.

# min rust version
1.38
