FROM scratch

COPY .well-known/acme-challenge /.well-known/acme-challenge/
COPY target/x86_64-unknown-linux-musl/release/redirect_https /redirect_https

ENV BIND_URL="127.0.0.1:80"
EXPOSE 80

ENTRYPOINT ["/redirect_https"]